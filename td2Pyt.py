import collections

Carte = collections.namedtuple('Carte', ['rang','couleur'])
class JeuCartes:
    rangs = [str(n) for n in range(2, 11)] + list('VDRA')
    couleurs ='pique coeur carreau trèfle'.split()

    def __init__(self):
        self._cartes = [Carte(rang, couleur) for couleur in self.couleurs for rang in self.rangs]
        
    def __len__(self):
        return len(self._cartes)
    """
    def __list__(self):
        return list(self._cartes)
    """

    def __getitem__(self, i):
        return self._cartes[i]

    def __delitem__(self, i):
        del self._cartes[i]

    def __iter__(self):
        return iter(self._cartes)
        #for c in self._cartes:
        #   yield c
    
jc= JeuCartes()
vp = Carte(rang='V', couleur='pique')
len(jc)
l = list(jc)
l[1]
l[52]
jc[12::13]

def tri_bridge(carte):
    coul_val = dict(trèfle=0, carreau=1, coeur=2, pique=3)
    try:
        return (int(carte.rang), coul_val[carte.couleur])
    except:
        cout_rang = {
            'V': 11,
            'D': 12,
            'R': 13,
            'A': 14
        }
        return (cout_rang[carte,rang], coul_val[carte.couleur])

sorted_jc = sorted(jc, key=tri_bridge)

class Vecteur:
    def __init__(self,x, y):
        self.x, self.y = x, y
    
    def __str__(self):
        return '({} ; {})'.format(self.x, self.y)

    def __repr__(self):
        return 'Vecteur ; ' + str(self)

    def __add__(self, other):
        pass
    
    def __nul__(self, other):
        return Vecteur(self.x * other.x, self.y * other.y)
