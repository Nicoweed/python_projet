import re
import requests
import csv
from bs4 import BeautifulSoup

url =  'https://stackoverflow.com/questions/tagged/beautifulsoup'
r = requests.get(url)
soup = BeautifulSoup(r.text, "lxml")

#Recherche par nom de balise
soup.find('p')
soup.find_all('p')

#Retourne les balises de 2 caractères
soup.find_all(re.compile(r'..'))

#idem par fonction
def f(tag):
    return len(tag.name) == 2

soup.find_all(f)

#Recherche par propritérés CSS
soup.select('.biglink')

#Recherche par nom + valeur d'un attribut
soup.find('a', href = re.compile('Python'))

#Recherche par propriété css + limite de portée
uneliste = soup.select('.question-hyperlink', limit = 10)

desVotes = soup.select('.vote-count-post', limit= 10)

desRep = soup.select('.status', limit = 10)

#Récupération des éléments précédents dans un .csv
lesQues=[]
lesVotes=[]
lesRep=[]

file = open('data.csv', 'w')
for i in range(0,10):
    lesQues.append(uneliste[i].get_text())
    lesVotes.append((desVotes[i].find('strong')).find(string=re.compile("-?[0-9]*")))
    lesRep.append((desRep[i].find('strong')).find(string=re.compile("-?[0-9]*")))
    writer = csv.writer(file, delimiter = '/')
    writer.writerow((lesQues[i], lesVotes[i], lesRep[i]))

file.close()
print(lesQues)
print(lesVotes)
print(lesRep)

unDico = {"submit-form": "", "catalog": "catalogue-2015-2016", "degree": "DP"}
uneRequete = requests.post("http://formation.univ-orleans.fr/fr/formation/rechercher-une-formation.html#nav", data=unDico)
formations = []
newSoup = BeautifulSoup(uneRequete.text, "lxml")
for node in newSoup.find_all('li', class_='hit'):
    for formation in node.findAll('strong'):
        formations.append(''.join(formation.findAll(text=True)))
print(formations)

def get_def(unFormat):
    URL ='http://services.aonaware.com/DictService/Default.aspx?action=define&dict=wn&query={0}'.format(unFormat)
    html = requests.get(URL).text
    realSoup = BeautifulSoup(html, "lxml")
    definitions=[]
    for node in soup.find_all('pre',attrs={"class":None}):
        definitions.append(''.join(node.findAll(text=True)))
    return definitions

rows=[]
with open('vocabulary.txt') as f:
    rows=f.readlines() 

with open('definitions.txt','w') as d:
    for mot in rows:
        d.write(get_def(mot)[0])