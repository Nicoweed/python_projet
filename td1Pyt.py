def test_nose1():
    dico={"Zickler" : 10, "Roux" : 20, "Lucasque" : 50, "Roux" : 5, "Zickler" : 10}
    valeurs=list(dico.values())
    total=0
    for i in range(len(valeurs)):
        total=total+i
    return total

def somme_personne(l, p):
    """
    cles=list(l.keys())
    total=0
    for i in range(len(l)):
        if i is p:
            total=total+l[i]
    return total
    """
    return sum([d for (e, d) in l if e == p])
    
def somme_par_personne(l):
    """
    for i in range(len(cles)):
        total=somme_personne(l, i)
        print(i, total)
        cles.remove(i)
        """
        personnes = set([p for p, _ in l])
    return [(p, somme_personne(l, p)) for p in personnes]

def avoir_par_personne(l):
    total= somme_personne(l)
    dep_par_per = somme_par_personne(l)
    dep_opti = total / len(dep_par_per)
    return [(p, dep_opti - d) for p, d in dep_par_per]

test_nose1()
dico1={"Zickler" : 10, "Roux" : 20, "Lucasque" : 50, "Roux" : 5, "Zickler" : 10}
personne1="Roux"    
somme_personne(dico1, personne1)  
somme_par_personne(dico1)